# ember-cli-spreadsheet

v0.0.3

This addon wraps the [sheetjs js-xlsx](https://github.com/SheetJS/js-xlsx) library for ember as a component.

## Use in your Ember CLI project

```javascript
    ember install https://bitbucket.org/amrdevelopment/ember-cli-spreadsheet
```

From inside a handlebars template you can inject the component like so:

```hbs
    {{ember-spreadsheet}}
```

If you would like, you could instead use the component in its block format.

```hbs
    {{#ember-spreadsheet}}
        <input type="file" accept={{_accept}} multiple={{allowMultipleFiles}} />

        ...
    {{/ember-spreadsheet}}
```

If you are passing an object into the component for the `workbooks` property (which holds all the parsed workbooks), import the "ember-spreadsheet-workbooks" mixin and use it to create a special workbooks object as seen in our example below:

```javascript
import Ember from 'ember';
import workbooksMixin from '../mixins/ember-spreadsheet-workbooks';

const WorkbooksObject = Ember.Object.extend(workbooksMixin);

export default Ember.Controller.extend({
    jsonOut: '',
    workbooks: WorkbooksObject.create(),
    actions: {
        afterImport() {
            let workbooks = this.get('workbooks');

            if (workbooks) {
                let jsonWorkbooks = Ember.A();

                workbooks.forEach((workbook, workbookName) => {
                    let jsonWorkbook = {},
                            jsonSheets = {},
                            sheetNames = workbook.SheetNames;

                    sheetNames.forEach((sheetName) => {
                        jsonSheets[sheetName] = sheet_to_json(workbook.Sheets[sheetName]);
                    });

                    jsonWorkbook[workbookName] = jsonSheets;
                    jsonWorkbooks.pushObject(jsonWorkbook);
                });

                this.set('jsonOut', JSON.stringify(jsonWorkbooks, null, 2));
            }
        }
    }
});
```


___

## Contributing


### Installation

* `git clone` this repository
* `npm install`
* `bower install`

### Running

* `ember server`
* Visit your app at http://localhost:4200.

### Running Tests

* `npm test` (Runs `ember try:testall` to test your addon against multiple Ember versions)
* `ember test`
* `ember test --server`

### Building

* `ember build`

For more information on using ember-cli, visit [http://www.ember-cli.com/](http://www.ember-cli.com/).
