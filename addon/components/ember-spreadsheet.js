import Ember from 'ember';
import layout from '../templates/components/ember-spreadsheet';
import computed from 'ember-new-computed';
import workbooksMixin from '../mixins/ember-spreadsheet-workbooks';
import { read } from 'xlsx';

const MIME_TYPES = {
	XLS: 'application/vnd.ms-excel',
	XLSX: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	XLSB: 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
	ODS: 'application/vnd.oasis.opendocument.spreadsheet',
	CSV: 'text/csv'
};

const WorkbooksObject = Ember.Object.extend(workbooksMixin);

export default Ember.Component.extend({
	layout,
	classNames: ['ember-spreadsheet'],

	////////////////////////////////////
	// Options Passed in to Component //
	////////////////////////////////////

	/**
	 * This determines what type of files can be imported.
	 *
	 * @property allowedFileTypes
	 * @default [MIME_TYPES.XLS, MIME_TYPES.XLSX, MIME_TYPES.XLSB, MIME_TYPES.ODS, MIME_TYPES.CSV]
	 * @type Ember.NativeArray
	 */
	allowedFileTypes: Ember.A([MIME_TYPES.XLS, MIME_TYPES.XLSX, MIME_TYPES.XLSB, MIME_TYPES.ODS, MIME_TYPES.CSV]),

	/**
	 * This dertimines if a user is able to upload more than one file at a time.
	 *
	 * @property allowMultipleFiles
	 * @default false
	 * @type Boolean
	 */
	allowMultipleFiles: false,

	/**
	 * If this property is set to true, then the file will be imported and parsed
	 * without the user having to click an "import" button.
	 *
	 * @property automaticImport
	 * @default false
	 * @type Boolean
	 */
	automaticImport: false,

	/**
	 * If this property is set to true, then the user will be prevented from uploading a file.
	 * By default, the the user will always be able to upload unless otherwise specified.
	 *
	 * @property disableImport
	 * @default  false
	 * @type Boolean
	 */
	disableImport: false,

	///////////////////////////////////
	// Internal Properties and State //
	///////////////////////////////////

	/**
	 * This is the array of raw File objects (https://developer.mozilla.org/en-US/docs/Web/API/File).
	 *
	 * @property files
	 * @default []
	 * @type Ember.NativeArray
	 */
	files: Ember.A(),

	/**
	 * The workbooks object is made up of each parsed workbook that was imported.
	 *
	 * @type {Ember.Object}
	 */
	workbooks: WorkbooksObject.create(),

	/**
	 * The import button will be shown when this true.
	 *
	 * @property _showImportButton
	 * @private
	 * @readOnly
	 * @default false
	 * @type Boolean
	 */
	_showImportButton: computed('files', {
		get() {
			const files = this.get('files');
			let automaticImport = this.get('automaticImport');

			return (files.length && !automaticImport) ? true : false;
		}
	}).readOnly(),

	/**
	 * Used to tell the input element what file types ot limit the search for.
	 *
	 * @property _accept
	 * @private
	 * @readOnly
	 * @default ''
	 * @type String
	 */
	_accept: computed('allowedFileTypes', {
		get() {
			let allowedFileTypes = this.get('allowedFileTypes');
			return allowedFileTypes.join(',');
		}
	}).readOnly(),

	////////////////////
	// Public Methods //
	////////////////////
	
	/**
	 * Checks to see if file(s) chosen by user follow parameters given.
	 *
	 * @method validateFiles
	 * @return {Object} object has a failed and reason property
	 */
	validateFiles() {
		let xlsx = this.get('xlsx'),
				files = this.get('files'),
				allowedFileTypes = this.get('allowedFileTypes'),
				allowMultipleFiles = this.get('allowMultipleFiles');

		if (!files) {
			return {
				failed: true,
				reason: 'ember-spreadsheet: Validation of files before import failed because no files were found.'
			};
		}

		if (!allowMultipleFiles && files.length > 1) {
			return {
				failed: true,
				reason: 'ember-spreadsheet: Validation of files before import failed because more files were selected than allowed.'
			}
		}

		// check that file types match allowed file types
		for (let i = 0; i < files.length; ++i) {
			if (!allowedFileTypes.contains(files[i].type)) {
				return {
					failed: true,
					reason: 'ember-spreadsheet: Validation of files before import failed because one or more of the selected files was a disallowed file type.'
				}
			}
		}

		return {
			failed: false,
			reason: ''
		};
	},


	/////////////////////
	// Private Methods //
	/////////////////////

	/**
	 * This function parses/reads selected files into workbook objects.
	 *
	 * @method _importFiles
	 * @private
	 */
	_importFiles() {
		return new Ember.RSVP.Promise((resolve, reject) => {
			let workbooks = this.get('workbooks'),
					files = this.get('files'),
					validateFiles = this.validateFiles();

			if (validateFiles.failed) {
				throw new Error(validateFiles.reason);
			}

			for (let i = 0; i < files.length; ++i) {
				let reader = new FileReader(),
						file = files[i],
						fileName = file.name;

				reader.onload = (e) => {
					let data = this._base64ArrayBuffer(e.target.result);

					let workbook = read(data, {type: 'base64'});
					workbooks.addWorkbook(fileName, workbook);

					if (i == files.length - 1) {
						resolve();
					}
				};

				reader.readAsArrayBuffer(file);
			}
		});
	},

	/**
	 * Converts an ArrayBuffer directly to base64
	 * Credit https://gist.github.com/jonleighton/958841
	 *
	 * @method _base64ArrayBuffer
	 * @private
	 * @returns base64 encoded string
	*/
	_base64ArrayBuffer(arrayBuffer) {
	  let base64    = ''
	  let encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

	  let bytes         = new Uint8Array(arrayBuffer)
	  let byteLength    = bytes.byteLength
	  let byteRemainder = byteLength % 3
	  let mainLength    = byteLength - byteRemainder

	  let a, b, c, d
	  let chunk

	  // Main loop deals with bytes in chunks of 3
	  for (let i = 0; i < mainLength; i = i + 3) {
	    // Combine the three bytes into a single integer
	    chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

	    // Use bitmasks to extract 6-bit segments from the triplet
	    a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
	    b = (chunk & 258048)   >> 12 // 258048   = (2^6 - 1) << 12
	    c = (chunk & 4032)     >>  6 // 4032     = (2^6 - 1) << 6
	    d = chunk & 63               // 63       = 2^6 - 1

	    // Convert the raw binary segments to the appropriate ASCII encoding
	    base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
	  }

	  // Deal with the remaining bytes and padding
	  if (byteRemainder == 1) {
	    chunk = bytes[mainLength]

	    a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

	    // Set the 4 least significant bits to zero
	    b = (chunk & 3)   << 4 // 3   = 2^2 - 1

	    base64 += encodings[a] + encodings[b] + '=='
	  } else if (byteRemainder == 2) {
	    chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

	    a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
	    b = (chunk & 1008)  >>  4 // 1008  = (2^6 - 1) << 4

	    // Set the 2 least significant bits to zero
	    c = (chunk & 15)    <<  2 // 15    = 2^4 - 1

	    base64 += encodings[a] + encodings[b] + encodings[c] + '='
	  }
	  
	  return base64
	},

	actions: {
		/**
		 * This method exposes the private method _importFiles as an action.
		 *
		 * @method importFiles
		 */
		importFiles() {
			this._importFiles().then(() => {
				this.sendAction('afterImport');
			});
		},

		/**
		 * This method retrieves the FileList object and sets the files property with it.
		 *
		 * @param {Object} e - The event object.
		 */
		retrieveFile(e) {
			this.set('files', e.target.files);

			if (this.get('automaticImport')) {
				this.send('importFiles');
			}
		}
	}
});
