import Ember from 'ember';

export default Ember.Mixin.create({
	/**
	 * Each key is the name of the workbook
	 * 
	 * @property workbooks
	 * @private
	 * @default {}
	 * @type {Map}
	 */
	_workbooks: null,

	/**
	 * Overriding Ember.Mixin.init. Setup is done here.
	 *
	 * @method init
	 */
	init() {
		this._super(...arguments);
		this.set('_workbooks', new Map());
	},

	/**
	 * Gets the workbook object based on the name.
	 * See the https://github.com/SheetJS/js-xlsx#working-with-the-workbook for details on
	 * how to work with the objects.
	 *
	 * @method getWorkbook
	 * @param  {String} name - the name of the workbook (without extension)
	 * @return {Object} workbook
	 */
	getWorkbook(name) {
		let workbooks = this.get('_workbooks');
		return workbooks.get(name);
	}, 

	/**
	 * Adds a workbook to the public and private workbooks vars.
	 *
	 * @method addWorkbook
	 * @param {String} fileName - the name of the workbook/file
	 * @param {Object} workbook - the workbook object
	 */
	addWorkbook(fileName, workbook) {
		let workbooks = this.get('_workbooks');

		let fileNameArr = fileName.split('.'),
				name = fileNameArr[0],
				extension = fileNameArr[1];

		workbooks.set(name, workbook);
	},

	/**
	 * Removes a workbook from the workbooks object.
	 *
	 * @method removeWorkbook
	 * @param {String} key - the name of the workbook to remove
	 */
	removeWorkbook(name) {
		let workbooks = this.get('_workbooks');

		workbooks.propertyWillChange(name);
		workbooks.delete(name);
		workbooks.propertyDidChange(name);
	},

	/**
	 * Clears the underlying map of workbooks.
	 *
	 * @method clear
	 */
	clear() {
		let workbooks = this.get('_workbooks');
		workbooks.clear();
	},

	/**
	 * The forEach() method executes a provided function once per each workbook in the underlying
	 * Map object, in insertion order.
	 *
	 * @method forEach
	 * @param  {Function} callback - Function to execute for each element. Recieves three arguments: 
	 *                             	 the element value, the element key, and the map object being traversed
	 * @param  {Object}   thisArg  - value to use as 'this' when executing the callback
	 * 
	 */
	forEach(callback, thisArg) {
		let workbooks = this.get('_workbooks');
		workbooks.forEach(callback, thisArg);
	},

	/**
	 * Checks if the underlying workbooks map contains the workbook.
	 * @param  {String} name - the name of the workbook (without extension)
	 * @return {Boolean} returns true if the workbook exists
	 */
	containsFile(name) {
		let workbooks = this.get('_workbooks');
		return workbooks.has(name);
	}
});
