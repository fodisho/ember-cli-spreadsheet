/* jshint node: true */
'use strict';

module.exports = {
  name: 'ember-cli-spreadsheet',

  included: function(app) {
  	this._super.included(app);

  	// see: https://github.com/ember-cli/ember-cli/issues/3718
    if (typeof app.import !== 'function' && app.app) {
      app = app.app;
    }

  	app.import(app.bowerDirectory + '/js-xlsx/dist/xlsx.core.min.js');
  	app.import('vendor/shims/js-xlsx.js');
  }
};
