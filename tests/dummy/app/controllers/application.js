import Ember from 'ember';
import workbooksMixin from '../mixins/ember-spreadsheet-workbooks';
import { utils as xlsxUtils } from 'xlsx';

const sheet_to_json = xlsxUtils.sheet_to_json;
const WorkbooksObject = Ember.Object.extend(workbooksMixin);

export default Ember.Controller.extend({
	jsonOut1: '',
	jsonOut2: '',
	workbooks1: WorkbooksObject.create(),
	workbooks2: WorkbooksObject.create(),

	displayWorkbooks(workbooks, num) {
		if (workbooks) {
				let jsonWorkbooks = Ember.A();

				workbooks.forEach((workbook, workbookName) => {
					let jsonWorkbook = {},
							jsonSheets = {},
							sheetNames = workbook.SheetNames;

					sheetNames.forEach((sheetName) => {
						jsonSheets[sheetName] = sheet_to_json(workbook.Sheets[sheetName]);
					});

					jsonWorkbook[workbookName] = jsonSheets;
					jsonWorkbooks.pushObject(jsonWorkbook);
				});

				this.set(`jsonOut${num}`, JSON.stringify(jsonWorkbooks, null, 2));
			}
	},


	actions: {
		afterImport() {
			let workbooks1 = this.get('workbooks1'),
					workbooks2 = this.get('workbooks2');

			this.displayWorkbooks(workbooks1, 1);
			this.displayWorkbooks(workbooks2, 2);
		}
	}
});
