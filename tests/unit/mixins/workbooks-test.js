import Ember from 'ember';
import WorkbooksMixin from 'ember-cli-spreadsheet/mixins/ember-spreadsheet-workbooks';
import { module, test } from 'qunit';

module('Unit | Mixin | ember-spreadsheet-workbooks');

test('it can be instantiated', (assert) => {
  let WorkbooksObject = Ember.Object.extend(WorkbooksMixin);
  let subject = WorkbooksObject.create();
  assert.ok(subject);
});

test('it can add a workbook to the workbook map', (assert) => {
	let WorkbooksObject = Ember.Object.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let testWorkbookName = 'test1.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(testWorkbookName, testWorkbook);

	assert.ok(subject.get('workbooks').has(testWorkbookName));
});

test('it can remove a workbook from the map', (assert) => {
	let WorkbooksObject = Ember.Object.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let testWorkbookName = 'test1.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(testWorkbookName, testWorkbook);
	subject.removeWorkbook(testWorkbookName);

	assert.notOk(subject.get('workbooks').has(testWorkbookName));
});

test('it can get a workbook by name from the workbook map', (assert) => {
	let WorkbooksObject = Ember.Object.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let testWorkbookName = 'test1.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(testWorkbookName, testWorkbook);

	assert.ok(subject.getWorkbook('test1'));
});

test('it clears the workbook map', (assert) => {
	let WorkbooksObject = Ember.Object.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let testWorkbookName = 'test1.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(testWorkbookName, testWorkbook);
	subject.clear();

	assert.notOK(subject.get('_workbooks').size);
});

test('it can check, by name, if a workbook is in the workbook map', (assert) => {
	let WorkbooksObject = Ember.Object.extend(WorkbooksMixin);
	let subject = WorkbooksObject.create();
	let testWorkbookName = 'test1.xlsx';
	let testWorkbook = {
		Sheets: [],
		SheetNames: []
	};

	subject.addWorkbook(testWorkbookName, testWorkbook);

	assert.ok(subject.containsFile('test1'));
});

